/*
 * (C) Copyright 2014, 2015 Collabora Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 */

#include  <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <systemd/sd-login.h>

#include  <gio/gio.h>
#include  <glib.h>

int
main (int argc, char **argv)
{
  GDBusProxy *user_sd;
  GDBusProxy *login_session;
  GDBusConnection *conn;
  GError *error = NULL;
  GVariant *ret = NULL, *env;
  GVariantBuilder *b;
  uid_t uid = getuid();
  gchar *dbus_path = NULL;
  gchar *dbus_address = NULL;
  gchar *session_path = NULL;
  gchar *session_id = NULL;
  gchar *t;
  GSubprocess *compositor;
  gchar *wayland_socket;
  GKeyFile *key_file;
  gchar *launcher;
  int launcher_argc;
  gchar **launcher_argv;


  dbus_path = g_build_filename (g_get_user_runtime_dir(), "bus", NULL);
  dbus_address = g_strdup_printf ("unix:path=%s", dbus_path);

  conn = g_dbus_connection_new_for_address_sync (
    dbus_address,
    G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT |
      G_DBUS_CONNECTION_FLAGS_MESSAGE_BUS_CONNECTION,
    NULL,
    NULL,
    &error);

  if (conn == NULL)
    goto fail;

  user_sd = g_dbus_proxy_new_sync (conn,
    G_DBUS_PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES |
    G_DBUS_PROXY_FLAGS_DO_NOT_CONNECT_SIGNALS,
    NULL,
    "org.freedesktop.systemd1",
    "/org/freedesktop/systemd1",
    "org.freedesktop.systemd1.Manager",
    NULL,
    &error);

  if (user_sd == NULL)
    goto fail;

  if (sd_pid_get_session (getpid (), &session_id) < 0)
    goto fail;

  session_path = g_strdup_printf("/org/freedesktop/login1/session/%s",
    session_id);

  login_session = g_dbus_proxy_new_for_bus_sync (G_BUS_TYPE_SYSTEM,
    G_DBUS_PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES |
    G_DBUS_PROXY_FLAGS_DO_NOT_CONNECT_SIGNALS,
    NULL,
    "org.freedesktop.login1",
    session_path,
    "org.freedesktop.login1.Session",
    NULL,
    &error);

  if (login_session == NULL)
    goto fail;

  ret = g_dbus_proxy_call_sync (login_session,
    "Activate", g_variant_new("()"),
    G_DBUS_CALL_FLAGS_NONE,
    -1, NULL, &error);

  if (ret == NULL)
    goto fail;
  g_variant_unref (ret);

  key_file = g_key_file_new ();
  if (!g_key_file_load_from_dirs (key_file,
      "user-session-launcher/session.launcher",
      (const gchar **)g_get_system_config_dirs(),
      NULL,
      G_KEY_FILE_NONE,
      &error)) {
    g_error ("Couldn't find session.launcher keyfile");
    goto fail;
  }

  launcher = g_key_file_get_string (key_file,
    "Launcher", "Exec", &error);
  if (launcher == NULL)
    goto fail;

  if (!g_shell_parse_argv (launcher, &launcher_argc, &launcher_argv, &error))
    goto fail;

  compositor = g_subprocess_newv ((const gchar * const *) launcher_argv,
    G_SUBPROCESS_FLAGS_NONE,
    &error);

  g_strfreev(launcher_argv);

  wayland_socket = g_build_filename (g_get_user_runtime_dir (),
    "wayland-0", NULL);

  /* FIXME nasty busy wait for socket */
  while (!g_file_test (wayland_socket, G_FILE_TEST_EXISTS))
    g_usleep (G_USEC_PER_SEC/20);

  g_free (wayland_socket);

  ret = g_dbus_proxy_call_sync (user_sd,
    "StartUnit",
      g_variant_new ("(ss)",
        "chaiwala.target",
        "replace"),
    G_DBUS_CALL_FLAGS_NONE,
    -1, NULL, &error);

  if (ret == NULL)
    goto fail;

  g_variant_unref (ret);
  g_free (dbus_path);
  g_free (dbus_address);

  g_free (session_path);
  g_free (session_id);

  g_subprocess_wait (compositor, NULL, NULL);

  return 0;

fail:
  if (error)
      g_printerr ("Failed: %s\n", error->message);
  exit(-1);
}
